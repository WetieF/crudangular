import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Employee } from '../model/employee.model';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';


@Component({
  selector: 'app-display-employee',
  templateUrl: './display-employee.component.html',
  styleUrls: ['./display-employee.component.css']
})
export class DisplayEmployeeComponent implements OnInit {

  selectedEmployeeId: number;
  confirmDelete: boolean = false;

  /*panelExpended: boolean = true;
  isHidden: boolean = false; Accordion*/


  //private _employee: Employee;
  @Input() employee: Employee;

  @Input() searchTerm: string;

  @Output() notifyDelete: EventEmitter<number> = new EventEmitter<number>();

  /*set employee(val: Employee) {
    this._employee = val;
  }

  get employee() : Employee {
    return this._employee;
  }*/

  constructor(private _route: ActivatedRoute, private _router: Router, private _employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.selectedEmployeeId = +this._route.snapshot.paramMap.get('id')
    //console.log(this.selectedEmployeeId);
  }

  viewEmployee() {
    this._router.navigate(['/employees', this.employee.id],
      {
        queryParams: { 'searchTerm': this.searchTerm }
      });
  }

  editEmployee() {
    this._router.navigate(['/edit', this.employee.id]);
  }

  deleteEmployee() {
    this._employeeService.delete(this.employee.id).subscribe(() =>{
      console.log(`Employee with ID =  ${this.employee.id} deleted`);
    }, (err: any) => {
      console.log(err);
    });

    this.notifyDelete.emit(this.employee.id);
  }

}
