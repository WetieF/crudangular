import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../model/employee.model';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Department } from '../model/department.model';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  public previewPhoto: boolean = false;

  datePickerConfig: Partial<BsDatepickerConfig>;


  employee: Employee;


  departments: Department[] = [
    { id: 1, name: 'Help Desk' },
    { id: 2, name: 'IT' },
    { id: 3, name: 'HR' },
    { id: 4, name: 'Dr.' }
  ];


  constructor(private _route: ActivatedRoute, private _employeeService: EmployeeService) {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        dateInputFormat: 'DD/MM/YYYY'
      });
  }

  togglePhotoPreview() {
    this.previewPhoto = !this.previewPhoto;
  }

  ngOnInit(): void {
    /*const id= this._route.snapshot.params['id'];
    this._employeeService.getEmployee(id).subscribe((data) => {
      this.employee = data;
    });*/
    this._route.paramMap.subscribe((paramsMap) => {
      const id = +paramsMap.get('id');
      this._employeeService.getEmployee(id).subscribe((data) => {
        this.employee = data;
      })
    });
  }

  saveEmployee() {}

}
