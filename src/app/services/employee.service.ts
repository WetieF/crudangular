import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { Employee } from '../model/employee.model';

import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  public baseUrl: string="http://localhost:3000/employees";

  constructor(private httpClient: HttpClient) { }

  public getEmployees(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.baseUrl)
    .pipe(catchError(this.handleError));
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.log('Client Side Error: ', errorResponse.error.message);
    } else {
      console.log('Server Side Error: ', errorResponse);
    }

    return throwError('There is a problem with the service. we are notified & working on it. Please try again later');
  }

  public getEmployee(id: number): Observable<Employee> {
    return this.httpClient.get<Employee>(this.baseUrl + '/' + id);
  }

  public deleteEmployee(employeeId: number) {
   // return this.httpClient.delete<void>(`${this.baseUrl}/${employeeId}`); //`${this.baseUrl}/${id}`
    return this.httpClient.delete(this.baseUrl + "/" + employeeId);
  }

  public saveRessource(employee: Employee) : Observable<Employee> {
    
    if(employee.id === null) {
      return this.httpClient.post<Employee>(this.baseUrl, employee);
    } else {

    }
    
  }
  //deleteArticleById(articleId: string): Observable<number> {
   // return this.http.delete<number>(this.articleUrl + "/" + articleId)

   public delete(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.baseUrl + '/' + id)
    .pipe(catchError(this.handleError));
   }


}
