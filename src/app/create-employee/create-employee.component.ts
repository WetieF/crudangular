import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Employee } from '../model/employee.model';
import { Department } from '../model/department.model';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { EmployeeService } from '../services/employee.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  @ViewChild('employeeForm') public createEmployeeForm: NgForm; //me permet d'avoir acces a dirty pr le formulaire

  public previewPhoto: boolean = false; //pour photo

  datePickerConfig: Partial<BsDatepickerConfig>;

  panelTitle: string;

  employee: Employee; /* = {

    id: null,
    name: null,
    gender: null,
    email: '',
    phoneNumber: null,
    contactPreference: null,
    dateOfBirth: null,
    department: 'select', //null,
    isActive: null,
    photoPath: null,
  };*/

  departments: Department[] = [
    { id: 1, name: 'Help Desk' },
    { id: 2, name: 'IT' },
    { id: 3, name: 'HR' },
    { id: 4, name: 'Dr.' }
  ];

  constructor(private _emplService: EmployeeService, private _router: Router, private _route: ActivatedRoute) {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        dateInputFormat: 'DD/MM/YYYY'
      });
  }

  togglePhotoPreview() {
    this.previewPhoto = !this.previewPhoto;
  }

  ngOnInit(): void {

    this._route.paramMap.subscribe((paramsMap) => {
      const id = +paramsMap.get('id');
      /*this._emplService.getEmployee(id).subscribe((data) => {
        this.employee = data;
      })*/

      this.getEmployee(id);
    });
  }

  private getEmployee(id: number) {

    if (id === 0) {

      this.employee = {
        id: null,
        name: null,
        gender: null,
        email: '',
        phoneNumber: null,
        contactPreference: null,
        dateOfBirth: null,
        department: 'select', //null,
        isActive: null,
        photoPath: null,
      };

      this.panelTitle = "Create Employee";
      this.createEmployeeForm.reset();

    } else {

      this.panelTitle = "Edit Employee";
      this._emplService.getEmployee(id).subscribe((data) => {
        this.employee = data;
        //this.employee = Object.assign({}, data);
      }), (err: any) => {
        console.log(err);
      };
    }
  }

  saveEmployee(): void {
    if (this.employee.id == null) {
      //const newEmployee: Employee = Object.assign({}, this.employee); //rmployee in this place is reset to null it is why we have to create assign Object
      //avec suscribe plus besoin de newEmployee. on passe directement this.employee
      this._emplService.saveRessource(this.employee).subscribe((data) => {
        console.log(data);
        this.createEmployeeForm.reset(); //Reset de form
        this._router.navigate(['list']); //navigating user to list Road
      }, (err: any) => {
        console.log(err);
      })
    }
  }


}
