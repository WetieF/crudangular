import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListEmployeesComponent } from './list-employees/list-employees.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { CreateEmployeeCanDeactivateGuardService } from './create-employee/create-employee-can-deactivate-guard.service';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeListResolverService } from './list-employees/employee-list-resolver.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { EmployeeDetailsGuardService } from './employee-details/employee-datails-guard.service';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';

const routes: Routes = [
  {
    path: "list", component: ListEmployeesComponent,
    resolve: { employeeList: EmployeeListResolverService }
  },
  /* {
    path: "create", component: CreateEmployeeComponent,
    canDeactivate: [CreateEmployeeCanDeactivateGuardService]
  }, */
  {
    path: "edit/:id", component: CreateEmployeeComponent,
    canDeactivate: [CreateEmployeeCanDeactivateGuardService]
  },
  {
    path: "employees/:id", component: EmployeeDetailsComponent,
    canActivate: [EmployeeDetailsGuardService]
  },
  { path: 'edit/:id', component: EditEmployeeComponent},
  { path: "", redirectTo: "/list", pathMatch: "full" },
  { path: "notfound", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], //, {enableTracing: true}
  exports: [RouterModule]
})
export class AppRoutingModule { }
