import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../model/employee.model';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  private _id: number;

  employee: Employee;

  constructor(private _route: ActivatedRoute, private _emplService: EmployeeService, private _router: Router) { }

  ngOnInit(): void {
    this._route.paramMap.subscribe(params => {
      this._id = +params.get('id');
      this._emplService.getEmployee(this._id).subscribe((dataEmpl) => {
        this.employee = dataEmpl;
      }, (err: any) => {
        console.log(err);
      });
    })

    /*
    this._id = +this._route.snapshot.paramMap.get('id'); //cette ligne permet de lire le id passe ds l'url
    this._emplService.getEmployee(this._id) //+this._route.snapshot.params['id']
      .subscribe((dataEmpl) => {
        this.employee = dataEmpl;
      })*/
  }

  viewNextEmployee() {
    if (this._id < 10) {
      this._id = this._id + 1;
    } else {
      this._id = 1;
    }
    this._router.navigate(['/employees', this._id], { 
      queryParamsHandling : 'preserve'
    });
  }

}
