import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Employee } from '../model/employee.model';
import { EmployeeService } from '../services/employee.service';
import { ResolvedEmployeeList } from '../list-employees/resolved-employeelist.model';
    
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';


@Injectable()
export class EmployeeListResolverService implements Resolve<ResolvedEmployeeList> {

    constructor(private _emplService: EmployeeService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedEmployeeList> {
        return this._emplService.getEmployees()
        .pipe(
            map((employeeList) => new ResolvedEmployeeList(employeeList)),
            catchError((err: any) => of(new ResolvedEmployeeList(null, err)))
        );
    }
}