import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service'
import { Employee } from '../model/employee.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ResolvedEmployeeList } from './resolved-employeelist.model';

@Component({
  //selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {

  public listEmployees: Employee[];//stores the full list of employees

  public filteredEmployees: Employee[]; //permet de chercher les employees ds une liste d'employees, stores the filteredliste of employees

  error : string;

  //searchTerm: string;  avant pr etre utilse avec le Pipe ds ce cas pas besoin de get/set et de la vble filteredEmployees
  private _searchTerm: string;

  get searchTerm(): string {
    return this._searchTerm;
  }

  set searchTerm(val: string) {
    this._searchTerm = val;
    this.filteredEmployees = this.employeesFilter(val);
  }

  employeesFilter(searchString: string) {
    return this.listEmployees.filter(employee =>
      employee.name.toLowerCase().indexOf(searchString.toLowerCase()) !== -1);
  }

  constructor(private _employeeService: EmployeeService, private _router: Router, private _route: ActivatedRoute) {
    const resolvedEmployeeList: ResolvedEmployeeList = this._route.snapshot.data['employeeList'];

    if (resolvedEmployeeList.error == null) {
      this.listEmployees = resolvedEmployeeList.employeeList;
    } else {
      this.error = resolvedEmployeeList.error;
      
    }

    this._route.queryParamMap.subscribe((params) => {
      if (params.has('searchTerm')) {
        this.searchTerm = params.get('searchTerm');
      } else {
        this.filteredEmployees = this.listEmployees;
      }
    });
  }

  /*ngOnInit(): void {
    this._employeeService.getEmployees().subscribe((employeeData: Employee[]) => {
      this.listEmployees = employeeData;
      this.filteredEmployees = this.listEmployees;//this line because at the beginning filteredEmployees is empty
    }) 
  }*/

  //------------------------------------------------------



  ngOnInit(): void {
    /*this._employeeService.getEmployees().subscribe((employeeData: Employee[]) => {
      this.listEmployees = employeeData;

      this._route.queryParamMap.subscribe((params) => {
        if (params.has('searchTerm')) {
          this.searchTerm = params.get('searchTerm');
        } else {
          this.filteredEmployees = this.listEmployees;
        }
      });
    });*/
  }

  /*onClick(emplid: number) {
    this._router.navigate(['/employees', emplid], {
      queryParams: { 'searchTerm': this.searchTerm }
    }); //linksparameter array
    //the {} is an Object and the key of the object is queryParams the value is also and Object {'searchTerm = KEY': this.searchTerm (value)} 
  }*/


  onDeleteNotification(id: number) {
    const i = this.filteredEmployees.findIndex(e => e.id === id);
    if(i !== -1) {
      this.filteredEmployees.splice(i, 1);
    }
  }

}
