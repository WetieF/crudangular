import { PipeTransform, Pipe } from '@angular/core';
import { Employee } from '../model/employee.model';

@Pipe({
    name: 'employeeFilter'
})
export class EmployeeFilterPipe implements PipeTransform {
    //this method 2 params, first list of employees to filter, second param is the search term it self
    transform(listEmployees: Employee[], searchTerm: string): Employee[] {

        //return empty array if array is falsy and return the original array if search text is empty
        if (!listEmployees || !searchTerm) {
            return listEmployees;
        }

        return listEmployees.filter(employee =>
            employee.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1);
            /*indexOf Method is going to return -1 if the searchTerm is not found in the employee name,
             if searchTerm is found, it is going to return a number order than -1.*/

         /* convert the searchTerm to lower case
        searchTerm = searchTerm.toLowerCase();
        // retrun the filtered array
       return listEmployees.filter(item => {
            if (item && item[fieldName]) {
                return item[fieldName].toLowerCase().includes(searchText);
            }
            return false;
        });*/


    }
}